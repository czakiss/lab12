package com.lab12.sudoku.dto;

import java.util.List;

public class SudokuDto {


    private List<Integer> lineIds;
    private List<Integer> columnIds;
    private List<Integer> areasIds;

    public SudokuDto(List<Integer> lineIds, List<Integer> columnIds, List<Integer> areasIds) {
        this.lineIds = lineIds;
        this.columnIds = columnIds;
        this.areasIds = areasIds;
    }

    public List<Integer> getLineIds() {
        return lineIds;
    }

    public void setLineIds(List<Integer> lineIds) {
        this.lineIds = lineIds;
    }

    public List<Integer> getColumnIds() {
        return columnIds;
    }

    public void setColumnIds(List<Integer> columnIds) {
        this.columnIds = columnIds;
    }

    public List<Integer> getAreasIds() {
        return areasIds;
    }

    public void setAreasIds(List<Integer> areasIds) {
        this.areasIds = areasIds;
    }

    public boolean checkValidation() {
        if (this.areasIds.size() > 0 || this.columnIds.size() > 0 || this.lineIds.size() > 0) {
            return false;
        }

        return true;
    }
    @Override
    public String toString() {
        return "SudokuDto{" +
                "\n lineIds=" + lineIds +
                "\n columnIds=" + columnIds +
                "\n areasIds=" + areasIds +
                '}';
    }
}
