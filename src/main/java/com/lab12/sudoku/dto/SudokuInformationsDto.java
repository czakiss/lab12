package com.lab12.sudoku.dto;

import java.util.List;

public class SudokuInformationsDto {
    private List<List<Integer>> numbers;

    public SudokuInformationsDto(List<List<Integer>> numbers) {
        this.numbers = numbers;
    }

    public List<List<Integer>> getNumbers() {
        return numbers;
    }

}
