package com.lab12.sudoku.repository.interfaces;

import com.lab12.sudoku.dto.SudokuInformationsDto;

public interface SudokuRepository {

    SudokuInformationsDto getSudokuInformation();

}
