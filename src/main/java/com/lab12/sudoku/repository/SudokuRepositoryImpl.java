package com.lab12.sudoku.repository;

import com.lab12.sudoku.dto.SudokuInformationsDto;
import com.lab12.sudoku.repository.interfaces.SudokuRepository;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SudokuRepositoryImpl implements SudokuRepository {

    private static final String FILE_NAME = "sudokuFiles/sudoku.csv";
    private static final String SLITTER = "-";

    public static final Integer AREA_SIZE = 3;
    public static final Integer SUDOKU_LENGTH = AREA_SIZE*3;

    public static final Integer NUMBER_EMPTY = 0;


    private List<List<Integer>> getValues() {

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(FILE_NAME);
        List<List<Integer>> sudokuNumbers = new ArrayList<>();

        //try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
        try (BufferedReader br = new BufferedReader(new FileReader(FILE_NAME))) {

            String line;

            while ((line = br.readLine()) != null) {

                List<Integer> row = new ArrayList<>();
                String[] values = line.split(SLITTER);
                int rowSize = values.length;

                for (int x = 0; x < SUDOKU_LENGTH; x++) {

                    if (x < rowSize && values[x] != null && !values[x].equals("")) {
                        row.add(Integer.parseInt(values[x]));
                    } else {
                        row.add(NUMBER_EMPTY);
                    }

                }
                sudokuNumbers.add(row);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sudokuNumbers;
    }

    @Override
    public SudokuInformationsDto getSudokuInformation() {

        List<List<Integer>> sudokuNumbers = getValues();

        return new SudokuInformationsDto(sudokuNumbers);
    }
}
