package com.lab12.sudoku.rest;

import com.lab12.sudoku.dto.SudokuDto;
import com.lab12.sudoku.service.interfaces.SudokuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SudokuController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SudokuController.class);

    @Autowired
    private SudokuService sudokuService;

    @CrossOrigin
    @PostMapping("/api/sudoku/verify")
    public ResponseEntity<SudokuDto> verifySudoku() {

        SudokuDto sudokuDto = sudokuService.checkSudoku();

        LOGGER.info("[SUDOKU] --- checking\n " + sudokuDto);

        if (sudokuDto.checkValidation()) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(sudokuDto, HttpStatus.BAD_REQUEST);
        }

    }
}
