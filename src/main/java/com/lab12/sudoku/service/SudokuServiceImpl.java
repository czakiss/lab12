package com.lab12.sudoku.service;

import com.lab12.sudoku.dto.SudokuInformationsDto;
import com.lab12.sudoku.dto.SudokuDto;
import com.lab12.sudoku.repository.SudokuRepositoryImpl;
import com.lab12.sudoku.repository.interfaces.SudokuRepository;
import com.lab12.sudoku.service.interfaces.SudokuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class SudokuServiceImpl implements SudokuService {
    @Autowired
    private SudokuRepository sudokuRepository;



    private List<Integer> checkRows(List<List<Integer>> sudokuNumbers) {

        List<Integer> invalidIds = new ArrayList<>();
        int i = 0;
        for (List<Integer> row: sudokuNumbers) {
            List<Integer> fields = new ArrayList<>();
            for (Integer value: row) {
                if (fields.contains(value) && value != SudokuRepositoryImpl.NUMBER_EMPTY) {
                    invalidIds.add(i);
                } else {
                    fields.add(value);
                }
            }
            i++;
        }

        return invalidIds;
    }

    private List<Integer> checkColumns(List<List<Integer>> sudokuNumbers) {

        List<Integer> invalidIds = new ArrayList<>();

        for (int i = 0; i < SudokuRepositoryImpl.SUDOKU_LENGTH; i++) {
            List<Integer> columnElements = new ArrayList<>();
            for (int j = 0; j < SudokuRepositoryImpl.SUDOKU_LENGTH; j++) {
                Integer value = sudokuNumbers.get(j).get(i);
                if (value != SudokuRepositoryImpl.NUMBER_EMPTY && columnElements.contains(value)) {
                    invalidIds.add(j);
                } else {
                    columnElements.add(value);
                }
            }
        }

        return invalidIds;
    }

    private List<List<Integer>> createZones(List<List<Integer>> sudokuNumbers) {
        int areaSize = SudokuRepositoryImpl.AREA_SIZE;
        int areas = SudokuRepositoryImpl.SUDOKU_LENGTH / areaSize;
        List<List<Integer>> zonesList = new ArrayList<>();
        for (int i = 0; i < areas; i++) {
            for (int j = 0; j < areas; j++) {
                zonesList.add(Arrays.asList(

                        sudokuNumbers.get(i * areaSize).get(j * areaSize),
                        sudokuNumbers.get(i * areaSize).get(j * areaSize + 1),
                        sudokuNumbers.get(i * areaSize).get(j * areaSize + 2),

                        sudokuNumbers.get(i * areaSize + 1).get(j * areaSize),
                        sudokuNumbers.get(i * areaSize + 1).get(j * areaSize + 1),
                        sudokuNumbers.get(i * areaSize + 1).get(j * areaSize + 2),

                        sudokuNumbers.get(i * areaSize + 2).get(j * areaSize),
                        sudokuNumbers.get(i * areaSize + 2).get(j * areaSize + 1),
                        sudokuNumbers.get(i * areaSize + 2).get(j * areaSize + 2)
                ));
            }
        }

        return zonesList;
    }

    private List<Integer> checkAreas(List<List<Integer>> sudokuNumbers) {
        List<List<Integer>> sectors = createZones(sudokuNumbers);

        return checkRows(sectors);
    }

    @Override
    public SudokuDto checkSudoku() {
        SudokuInformationsDto sudokuInformation = sudokuRepository.getSudokuInformation();
        List<List<Integer>> sudokuNumbers = sudokuInformation.getNumbers();

        List<Integer> invalidRows = checkRows(sudokuNumbers);
        List<Integer> invalidColumns = checkColumns(sudokuNumbers);
        List<Integer> invalidAreas = checkAreas(sudokuNumbers);

        return new SudokuDto(invalidRows, invalidColumns, invalidAreas);
    }

}
