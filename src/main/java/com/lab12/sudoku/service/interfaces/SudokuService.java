package com.lab12.sudoku.service.interfaces;

import com.lab12.sudoku.dto.SudokuDto;

public interface SudokuService {

    SudokuDto checkSudoku();

}
